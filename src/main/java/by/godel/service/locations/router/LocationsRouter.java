package by.godel.service.locations.router;

import by.godel.service.locations.rest.Handler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class LocationsRouter {

    private static final String CONTEXT_PATH = "/api/locations";
    private static final String PING_PATH = "/ping";

    @Bean
    public RouterFunction<ServerResponse> route(Handler handler) {

        return RouterFunctions
                .route(RequestPredicates.GET(CONTEXT_PATH + PING_PATH).
                        and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
                        handler::handle);
    }
}
